package routes

import (
    "github.com/gin-gonic/gin"
    "gorm.io/gorm"

    "fp_go_sanbercode30/controllers"
    "fp_go_sanbercode30/middlewares"

    swaggerFiles "github.com/swaggo/files"     // swagger embed files
    ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware
)

func SetupRouter(db *gorm.DB) *gin.Engine {
    r := gin.Default()

    // set db to gin context
    r.Use(func(c *gin.Context) {
        c.Set("db", db)
    })

    r.POST("/register", controllers.Register)
    r.POST("/login", controllers.Login)
    PasswordMiddlewareRoute := r.Group("/changepassword/")
    PasswordMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    PasswordMiddlewareRoute.PATCH("/:id", controllers.ChangePassword)

    r.GET("/phones", controllers.GetAllPhone)
    r.GET("/phones/:id", controllers.GetPhoneById)
    r.GET("/phones/:id/ratings", controllers.GetRatingsByPhoneId)
    r.GET("/phones/:id/opinions", controllers.GetOpinionsByPhoneId)
    r.GET("/phones/:id/prices", controllers.GetPricesByPhoneId)
    r.GET("/phones/:id/images", controllers.GetImagesByPhoneId)
    r.GET("/phones/:id/reviews", controllers.GetReviewsByPhoneId)
    phonesMiddlewareRoute := r.Group("/phones/")
    phonesMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    phonesMiddlewareRoute.POST("/", controllers.CreatePhone)
    phonesMiddlewareRoute.PATCH("/:id", controllers.UpdatePhone)
    phonesMiddlewareRoute.DELETE("/:id", controllers.DeletePhone)

    r.GET("/phone-brands", controllers.GetAllBrand)
    r.GET("/phone-brands/:id", controllers.GetBrandById)
    r.GET("/phone-brands/:id/phones", controllers.GetPhonesByBrandId)
    phoneBrandsMiddlewareRoute := r.Group("/phone-brands/")
    phoneBrandsMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    phoneBrandsMiddlewareRoute.POST("/", controllers.CreateBrand)
    phoneBrandsMiddlewareRoute.PATCH("/:id", controllers.UpdateBrand)
    phoneBrandsMiddlewareRoute.DELETE("/:id", controllers.DeleteBrand)

    r.GET("/ratings", controllers.GetAllRating)
    r.GET("/ratings/:id", controllers.GetRatingById)
    ratingsMiddlewareRoute := r.Group("/ratings/")
    ratingsMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    ratingsMiddlewareRoute.POST("/", controllers.CreateRating)
    ratingsMiddlewareRoute.PATCH("/:id", controllers.UpdateRating)
    ratingsMiddlewareRoute.DELETE("/:id", controllers.DeleteRating)

    r.GET("/opinions", controllers.GetAllOpinion)
    r.GET("/opinions/:id", controllers.GetOpinionById)
    opinionsMiddlewareRoute := r.Group("/opinions/")
    opinionsMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    opinionsMiddlewareRoute.POST("/", controllers.CreateOpinion)
    opinionsMiddlewareRoute.PATCH("/:id", controllers.UpdateOpinion)
    opinionsMiddlewareRoute.DELETE("/:id", controllers.DeleteOpinion)

    r.GET("/editor-reviews", controllers.GetAllEditorReview)
    r.GET("/editor-reviews/:id", controllers.GetEditorReviewById)
    editorReviewsMiddlewareRoute := r.Group("/editor-reviews/")
    editorReviewsMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    editorReviewsMiddlewareRoute.POST("/", controllers.CreateEditorReview)
    editorReviewsMiddlewareRoute.PATCH("/:id", controllers.UpdateEditorReview)
    editorReviewsMiddlewareRoute.DELETE("/:id", controllers.DeleteEditorReview)

    r.GET("/phone-prices", controllers.GetAllPhonePrice)
    r.GET("/phone-prices/:id", controllers.GetPhonePriceById)
    phonePricesMiddlewareRoute := r.Group("/phone-prices/")
    phonePricesMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    phonePricesMiddlewareRoute.POST("/", controllers.CreatePhonePrice)
    phonePricesMiddlewareRoute.PATCH("/:id", controllers.UpdatePhonePrice)
    phonePricesMiddlewareRoute.DELETE("/:id", controllers.DeletePhonePrice)

    r.GET("/phone-images", controllers.GetAllPhoneImage)
    r.GET("/phone-images/:id", controllers.GetPhoneImageById)
    phoneImagesMiddlewareRoute := r.Group("/phone-images")
    phoneImagesMiddlewareRoute.Use(middlewares.JwtAuthMiddleware())
    phoneImagesMiddlewareRoute.POST("/", controllers.CreatePhoneImage)
    phoneImagesMiddlewareRoute.PATCH("/:id", controllers.UpdatePhoneImage)
    phoneImagesMiddlewareRoute.DELETE("/:id", controllers.DeletePhoneImage)

    r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

    return r
}