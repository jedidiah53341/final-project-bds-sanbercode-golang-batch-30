package models

import (
    "time"
)

type (
    //Phone
    PhoneImage struct {
        ID          uint      `gorm:"primary_key" json:"id"`
        ImageSrc        string    `json:"image_src"`
        CreatedAt   time.Time `json:"created_at"`
        UpdatedAt   time.Time `json:"updated_at"`
        UserID      uint   `json:"userID"`
        User      User   `json:"-"`
        PhoneID      uint   `json:"phoneID"`
        Phone      Phone   `json:"-"`
    }
)