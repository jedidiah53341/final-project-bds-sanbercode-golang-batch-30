package models

import (
    "time"
)

type (
    //Phone
    EditorReview struct {
        ID          uint      `gorm:"primary_key" json:"id"`
        Content        string    `json:"content"`
        CreatedAt   time.Time `json:"created_at"`
        UpdatedAt   time.Time `json:"updated_at"`
        UserID      uint   `json:"userID"`
        User      User   `json:"-"`
        PhoneID      uint   `json:"phoneID"`
        Phone      Phone   `json:"-"`
    }
)