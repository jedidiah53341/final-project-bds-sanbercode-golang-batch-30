package models

import (
    "time"
)

type (
    //Phone
    Rating struct {
        ID          uint      `gorm:"primary_key" json:"id"`
        UXScore        int    `json:"ux_score"`
        SpeedScore        int    `json:"speed_score"`
        GamingScore        int    `json:"gaming_score"`
        StandbyScore        int    `json:"standby_score"`
        CreatedAt   time.Time `json:"created_at"`
        UpdatedAt   time.Time `json:"updated_at"`
        UserID      uint   `json:"userID"`
        User      User   `json:"-"`
        PhoneID      uint   `json:"phoneID"`
        Phone      Phone   `json:"-"`
    }
)