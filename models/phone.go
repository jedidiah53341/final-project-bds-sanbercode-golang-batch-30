package models

import (
    "time"
)

type (
    //Phone
    Phone struct {
        ID          uint      `gorm:"primary_key" json:"id"`
        Name        string    `json:"name"`
        Year        int    `json:"year"`
        NetworkTechnology string    `json:"network_technology"`
        BodyDimensions        string    `json:"body_dimensions"`
        BodyWeight        string    `json:"body_weight"`
        BodyBuild        string    `json:"body_build"`
        BodySIM        string    `json:"body_sim"`
        DisplayType        string    `json:"display_type"`
        DisplaySize        string    `json:"display_size"`
        DisplayResolution        string    `json:"display_resolution"`
        PlatformOS        string    `json:"platform_os"`
        PlatformChipset        string    `json:"platform_chipset"`
        PlatformCPU        string    `json:"platform_cpu"`
        PlatformGPU        string    `json:"platform_gpu"`
        MemoryCard        string    `json:"memory_card"`
        MemoryInternal        string    `json:"memory_internal"`
        Camera        string    `json:"camera"`
        SoundLoudspeaker        string    `json:"sound_loudspeaker"`
        SoundJack        string    `json:"sound_jack"`
        CommsWLAN        string    `json:"comms_wlan"`
        CommsBluetooth        string    `json:"comms_bluetooth"`
        CommsGPS        string    `json:"comms_gps"`
        CommsUSB        string    `json:"comms_usb"`
        Battery        string    `json:"battery"`
        MiscColors        string    `json:"misc_colors"`
        MiscPrice        string    `json:"misc_price"`
        CreatedAt   time.Time `json:"created_at"`
        UpdatedAt   time.Time `json:"updated_at"`
        PhoneBrandID      uint   `json:"phoneBrandID"`
        PhoneBrand      PhoneBrand   `json:"-"`
        EditorReview      []EditorReview   `json:"-"`
        Ratings      []Rating   `json:"-"`
        Opinions      []Opinion   `json:"-"`
        PhonePrices      []PhonePrice   `json:"-"`
        PhoneImages      []PhoneImage   `json:"-"`
    }
)