package models

import (
    "time"
)

type (
    //Phone
    PhonePrice struct {
        ID          uint      `gorm:"primary_key" json:"id"`
        Currency        string    `json:"currency"`
        Price        float64    `json:"price`
        Source        string    `json:"source"`
        CreatedAt   time.Time `json:"created_at"`
        UpdatedAt   time.Time `json:"updated_at"`
        UserID      uint   `json:"userID"`
        User      User   `json:"-"`
        PhoneID      uint   `json:"phoneID"`
        Phone      Phone   `json:"-"`
    }
)