package controllers

import (
    "net/http"
    "time"
    "fp_go_sanbercode30/models"
    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
)

type opinionInput struct {
        Content     string    `json:"content"`
        UserID      uint   `json:"userID"`
        PhoneID      uint   `json:"phoneID"`
}

// GetAllOpinions godoc
// @Summary Dapatkan semua opinions.
// @Description Dapatkan semua opinions.
// @Tags Opinion
// @Produce json
// @Success 200 {object} []models.Opinion
// @Router /opinions [get]
func GetAllOpinion(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var Opinions []models.Opinion
    db.Find(&Opinions)

    c.JSON(http.StatusOK, gin.H{"data": Opinions})
}

// CreateOpinion godoc
// @Summary Buat sebuah opinion terhadap suatu model smartphone.
// @Description Buat sebuah opinion terhadap suatu model smartphone.
// @Tags Opinion
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body opinionInput true "body untuk membuat 1 Opinion"
// @Produce json
// @Success 200 {object} models.Opinion
// @Router /opinions [post]
func CreateOpinion(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)

    // Validate input
    var input opinionInput
    var user models.User
    var phone models.Phone
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    if err := db.Where("id = ?", input.UserID).First(&user).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "UserID not found!"})
        return
    }

    if err := db.Where("id = ?", input.PhoneID).First(&phone).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "PhoneID not found!"})
        return
    }

    // Create Opinion
    Opinion := models.Opinion{Content: input.Content, UserID: input.UserID, PhoneID: input.PhoneID }
    db.Create(&Opinion)

    c.JSON(http.StatusOK, gin.H{"data": Opinion})
}

// GetOpinionById godoc
// @Summary Dapatkan data opini berdasarkan id opini.
// @Description Dapatkan data opini berdasarkan id opini.
// @Tags Opinion
// @Produce json
// @Param id path string true "Opinion id"
// @Success 200 {object} models.Opinion
// @Router /opinions/{id} [get]
func GetOpinionById(c *gin.Context) { // Get model if exist
    var Opinion models.Opinion

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&Opinion).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": Opinion})
}

// UpdateOpinion godoc
// @Summary Update data opini berdasarkan id opini.
// @Description Update data opini berdasarkan id opini.
// @Tags Opinion
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Opinion id"
// @Param Body body opinionInput true "body untuk update an Opinion"
// @Success 200 {object} models.Opinion
// @Router /opinions/{id} [patch]
func UpdateOpinion(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var Opinion models.Opinion
    var user models.User
    var phone models.Phone
    if err := db.Where("id = ?", c.Param("id")).First(&Opinion).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input opinionInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    if err := db.Where("id = ?", input.UserID).First(&user).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "UserID not found!"})
        return
    }

    if err := db.Where("id = ?", input.PhoneID).First(&phone).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "PhoneID not found!"})
        return
    }

    var updatedInput models.Opinion
    updatedInput.Content = input.Content
    updatedInput.UserID = input.UserID
    updatedInput.PhoneID = input.PhoneID
    updatedInput.UpdatedAt = time.Now()

    db.Model(&Opinion).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": Opinion})
}

// DeleteOpinion godoc
// @Summary Hapus data opini berdasarkan id opini.
// @Description Hapus data opini berdasarkan id opini.
// @Tags Opinion
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Opinion id"
// @Success 200 {object} map[string]boolean
// @Router /opinion/{id} [delete]
func DeleteOpinion(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var Opinion models.Opinion
    if err := db.Where("id = ?", c.Param("id")).First(&Opinion).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&Opinion)

    c.JSON(http.StatusOK, gin.H{"data": true})
}