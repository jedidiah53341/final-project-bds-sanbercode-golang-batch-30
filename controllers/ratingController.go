package controllers

import (
    "net/http"
    "time"
    "fp_go_sanbercode30/models"
    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
)

type ratingInput struct {
        UXScore        int    `json:"ux_score"`
        SpeedScore        int    `json:"speed_score"`
        GamingScore        int    `json:"gaming_score"`
        StandbyScore        int    `json:"standby_score"`
        UserID      uint   `json:"userID"`
        PhoneID      uint   `json:"phoneID"`
}

// GetAllRatings godoc
// @Summary Dapatkan semua Ratings.
// @Description Dapatkan daftar Ratings.
// @Tags Rating
// @Produce json
// @Success 200 {object} []models.Rating
// @Router /ratings [get]
func GetAllRating(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var Ratings []models.Rating
    db.Find(&Ratings)

    c.JSON(http.StatusOK, gin.H{"data": Ratings})
}

// CreateRating godoc
// @Summary Membuat sebuah Rating.
// @Description Creating a new Rating.
// @Tags Rating
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body ratingInput true "body untuk membuat 1 Rating dari skala 1-100 bilangan bulat"
// @Produce json
// @Success 200 {object} models.Rating
// @Router /ratings [post]
func CreateRating(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)

    // Validate input
    var input ratingInput
    var user models.User
    var phone models.Phone
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    if err := db.Where("id = ?", input.UserID).First(&user).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "UserID not found!"})
        return
    }

    if err := db.Where("id = ?", input.PhoneID).First(&phone).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "PhoneID not found!"})
        return
    }

    if input.UXScore > 100 || input.SpeedScore > 100 || input.GamingScore > 100 || input.StandbyScore > 100 {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Mohon masukkan penilaian pada rentang nilai 0 hingga 100."})
        return
    }

    // Create Rating
    Rating := models.Rating{UXScore: input.UXScore, SpeedScore: input.SpeedScore, GamingScore: input.GamingScore, StandbyScore: input.StandbyScore, UserID: input.UserID, PhoneID: input.PhoneID }
    db.Create(&Rating)

    c.JSON(http.StatusOK, gin.H{"data": Rating})
}

// GetRatingById godoc
// @Summary Get Rating.
// @Description Dapatkan 1 Rating berdasarkan id.
// @Tags Rating
// @Produce json
// @Param id path string true "Rating id"
// @Success 200 {object} models.Rating
// @Router /ratings/{id} [get]
func GetRatingById(c *gin.Context) { // Get model if exist
    var Rating models.Rating

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&Rating).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": Rating})
}

// UpdateRating godoc
// @Summary Update Rating.
// @Description Update Rating berdasarkan id.
// @Tags Rating
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Rating id"
// @Param Body body ratingInput true "body untuk update an Rating"
// @Success 200 {object} models.Rating
// @Router /ratings/{id} [patch]
func UpdateRating(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var Rating models.Rating
    var user models.User
    var phone models.Phone
    if err := db.Where("id = ?", c.Param("id")).First(&Rating).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input ratingInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    if err := db.Where("id = ?", input.UserID).First(&user).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "UserID not found!"})
        return
    }

    if err := db.Where("id = ?", input.PhoneID).First(&phone).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "PhoneID not found!"})
        return
    }

    if input.UXScore > 100 || input.SpeedScore > 100 || input.GamingScore > 100 || input.StandbyScore > 100 {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Mohon masukkan penilaian pada rentang nilai 0 hingga 100."})
        return
    }

    var updatedInput models.Rating
    updatedInput.UXScore = input.UXScore
    updatedInput.SpeedScore = input.SpeedScore
    updatedInput.GamingScore = input.GamingScore
    updatedInput.StandbyScore = input.StandbyScore
    updatedInput.UserID = input.UserID
    updatedInput.PhoneID = input.PhoneID
    updatedInput.UpdatedAt = time.Now()

    db.Model(&Rating).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": Rating})
}

// DeleteRating godoc
// @Summary Hapus 1 Rating.
// @Description Hapus satu Rating berdasarkan id.
// @Tags Rating
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Rating id"
// @Success 200 {object} map[string]boolean
// @Router /rating/{id} [delete]
func DeleteRating(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var Rating models.Rating
    if err := db.Where("id = ?", c.Param("id")).First(&Rating).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&Rating)

    c.JSON(http.StatusOK, gin.H{"data": true})
}