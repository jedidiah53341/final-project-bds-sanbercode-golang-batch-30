package controllers

import (
    "net/http"
    "time"
    "fp_go_sanbercode30/models"
    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
)

type phoneImageInput struct {
        ImageSrc        string    `json:"image_src"`
        UserID      uint   `json:"userID"`
        PhoneID      uint   `json:"phoneID"`
}

// GetAllPhoneImages godoc
// @Summary Dapatkan semua gambar dari semua model smartphone.
// @Description Dapatkan semua gambar dari semua model smartphone.
// @Tags PhoneImage
// @Produce json
// @Success 200 {object} []models.PhoneImage
// @Router /phone-images [get]
func GetAllPhoneImage(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var PhoneImages []models.PhoneImage
    db.Find(&PhoneImages)

    c.JSON(http.StatusOK, gin.H{"data": PhoneImages})
}

// CreatePhoneImage godoc
// @Summary Masukkan sebuah gambar smartphone.
// @Description Untuk memasukkan gambar, upload gambar tersebut dan tempel url-nya di image_src.
// @Tags PhoneImage
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body phoneImageInput true "body untuk membuat 1 PhoneImage."
// @Produce json
// @Success 200 {object} models.PhoneImage
// @Router /phone-images [post]
func CreatePhoneImage(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)

    // Validate input
    var input phoneImageInput
    var user models.User
    var phone models.Phone
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    if err := db.Where("id = ?", input.UserID).First(&user).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "UserID not found!"})
        return
    }

    if err := db.Where("id = ?", input.PhoneID).First(&phone).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "PhoneID not found!"})
        return
    }

    // Create PhoneImage
    PhoneImage := models.PhoneImage{ImageSrc: input.ImageSrc,  UserID: input.UserID, PhoneID: input.PhoneID } 
    db.Create(&PhoneImage)

    c.JSON(http.StatusOK, gin.H{"data": PhoneImage})
}

// GetPhoneImageById godoc
// @Summary Dapatkan sebuah gambar berdasarkan id gambarnya.
// @Description Dapatkan sebuah gambar berdasarkan id gambarnya.
// @Tags PhoneImage
// @Produce json
// @Param id path string true "PhoneImage id"
// @Success 200 {object} models.PhoneImage
// @Router /phone-images/{id} [get]
func GetPhoneImageById(c *gin.Context) { // Get model if exist
    var PhoneImage models.PhoneImage

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&PhoneImage).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": PhoneImage})
}

// UpdatePhoneImage godoc
// @Summary Update sebuah gambar berdasarkan id gambarnya.
// @Description Update sebuah gambar berdasarkan id gambarnya.
// @Tags PhoneImage
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "PhoneImage id"
// @Param Body body phoneImageInput true "body untuk update an PhoneImage"
// @Success 200 {object} models.PhoneImage
// @Router /phone-images/{id} [patch]
func UpdatePhoneImage(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var PhoneImage models.PhoneImage
    var user models.User
    var phone models.Phone
    if err := db.Where("id = ?", c.Param("id")).First(&PhoneImage).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input phoneImageInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    if err := db.Where("id = ?", input.UserID).First(&user).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "UserID not found!"})
        return
    }

    if err := db.Where("id = ?", input.PhoneID).First(&phone).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "PhoneID not found!"})
        return
    }


    var updatedInput models.PhoneImage
    updatedInput.ImageSrc = input.ImageSrc
    updatedInput.UserID = input.UserID
    updatedInput.PhoneID = input.PhoneID
    updatedInput.UpdatedAt = time.Now()

    db.Model(&PhoneImage).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": PhoneImage})
}

// DeletePhoneImage godoc
// @Summary Hapus sebuah gambar berdasarkan id gambarnya.
// @Description Hapus sebuah gambar berdasarkan id gambarnya.
// @Tags PhoneImage
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "PhoneImage id"
// @Success 200 {object} map[string]boolean
// @Router /phone-images/{id} [delete]
func DeletePhoneImage(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var PhoneImage models.PhoneImage
    if err := db.Where("id = ?", c.Param("id")).First(&PhoneImage).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&PhoneImage)

    c.JSON(http.StatusOK, gin.H{"data": true})
}