package controllers

import (
    "net/http"
    "time"
    "fp_go_sanbercode30/models"
    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
)

type editorReviewInput struct {
        Content        string    `json:"content"`
        UserID      uint   `json:"userID"`
        PhoneID      uint   `json:"phoneID"`
}

// GetAllEditorReview godoc
// @Summary Dapatkan semua review untuk setiap model smartphone.
// @Description Dapatkan semua review untuk setiap model smartphone.
// @Tags EditorReview
// @Produce json
// @Success 200 {object} []models.EditorReview
// @Router /editor-reviews [get]
func GetAllEditorReview(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var EditorReviews []models.EditorReview
    db.Find(&EditorReviews)

    c.JSON(http.StatusOK, gin.H{"data": EditorReviews})
}

// CreateEditorReview godoc
// @Summary Buat 1 EditorReview.
// @Description Membuat 1 review.
// @Tags EditorReview
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body editorReviewInput true "body untuk membuat 1 EditorReview"
// @Produce json
// @Success 200 {object} models.EditorReview
// @Router /editor-reviews [post]
func CreateEditorReview(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    // Validate input
    var input editorReviewInput
    var user models.User
    var phone models.Phone
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    if err := db.Where("id = ?", input.UserID).First(&user).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "UserID not found!"})
        return
    }

    if err := db.Where("id = ?", input.PhoneID).First(&phone).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "PhoneID not found!"})
        return
    }

    // Create EditorReview
    EditorReview := models.EditorReview{Content: input.Content, UserID: input.UserID, PhoneID: input.PhoneID}
    db.Create(&EditorReview)

    c.JSON(http.StatusOK, gin.H{"data": EditorReview})
}

// GetEditorReviewById godoc
// @Summary Dapatkan sebuah data review berdasarkan id review.
// @Description Dapatkan sebuah data review berdasarkan id review.
// @Tags EditorReview
// @Produce json
// @Param id path string true "EditorReview id"
// @Success 200 {object} models.EditorReview
// @Router /editor-reviews/{id} [get]
func GetEditorReviewById(c *gin.Context) { // Get model if exist
    var EditorReview models.EditorReview

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&EditorReview).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": EditorReview})
}

// UpdateEditorReview godoc
// @Summary Update sebuah data review berdasarkan id review.
// @Description Update sebuah data review berdasarkan id review.
// @Tags EditorReview
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "EditorReview id"
// @Param Body body editorReviewInput true "body untuk update age EditorReview category"
// @Success 200 {object} models.EditorReview
// @Router /editor-reviews/{id} [patch]
func UpdateEditorReview(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var EditorReview models.EditorReview
    var user models.User
    var phone models.Phone
    if err := db.Where("id = ?", c.Param("id")).First(&EditorReview).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input editorReviewInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    if err := db.Where("id = ?", input.UserID).First(&user).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "UserID not found!"})
        return
    }

    if err := db.Where("id = ?", input.PhoneID).First(&phone).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "PhoneID not found!"})
        return
    }

    var updatedInput models.EditorReview
    updatedInput.Content = input.Content
    updatedInput.UserID = input.UserID
    updatedInput.PhoneID = input.PhoneID
    updatedInput.UpdatedAt = time.Now()

    db.Model(&EditorReview).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": EditorReview})
}

// DeleteEditorReview godoc
// @Summary Hapus sebuah data review berdasarkan id review.
// @Description Hapus sebuah data review berdasarkan id review.
// @Tags EditorReview
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "EditorReview id"
// @Success 200 {object} map[string]boolean
// @Router /editor-reviews/{id} [delete]
func DeleteEditorReview(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var EditorReview models.EditorReview
    if err := db.Where("id = ?", c.Param("id")).First(&EditorReview).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&EditorReview)

    c.JSON(http.StatusOK, gin.H{"data": true})
}