package controllers

import (
    "net/http"
    "time"
    "fp_go_sanbercode30/models"
    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
)

type phoneInput struct {
    Name              string `json:"name"`
    Year                int    `json:"year"`
    NetworkTechnology string    `json:"network_technology"`
    BodyDimensions        string    `json:"body_dimensions"`
    BodyWeight        string    `json:"body_weight"`
    BodyBuild        string    `json:"body_build"`
    BodySIM        string    `json:"body_sim"`
    DisplayType        string    `json:"display_type"`
    DisplaySize        string    `json:"display_size"`
    DisplayResolution        string    `json:"display_resolution"`
    PlatformOS        string    `json:"platform_os"`
    PlatformChipset        string    `json:"platform_chipset"`
    PlatformCPU        string    `json:"platform_cpu"`
    PlatformGPU        string    `json:"platform_gpu"`
    MemoryCard        string    `json:"memory_card"`
    MemoryInternal        string    `json:"memory_internal"`
    Camera        string    `json:"camera"`
    SoundLoudspeaker        string    `json:"sound_loudspeaker"`
    SoundJack        string    `json:"sound_jack"`
    CommsWLAN        string    `json:"comms_wlan"`
    CommsBluetooth        string    `json:"comms_bluetooth"`
    CommsGPS        string    `json:"comms_gps"`
    CommsUSB        string    `json:"comms_usb"`
    Battery        string    `json:"battery"`
    MiscColors        string    `json:"misc_colors"`
    MiscPrice        string    `json:"misc_price"`
    PhoneBrandID      uint   `json:"phoneBrandID"`
}

// GetAllPhones godoc
// @Summary Dapatkan semua model smartphone.
// @Description Dapatkan daftar Phones.
// @Tags Phone
// @Produce json
// @Success 200 {object} []models.Phone
// @Router /phones [get]
func GetAllPhone(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var Phones []models.Phone
    db.Find(&Phones)

    c.JSON(http.StatusOK, gin.H{"data": Phones})
}

// CreatePhone godoc
// @Summary Buat sebuah model smartphone.
// @Description Creating a new Phone.
// @Tags Phone
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body phoneInput true "body untuk membuat 1 Phone"
// @Produce json
// @Success 200 {object} models.Phone
// @Router /phones [post]
func CreatePhone(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)

    // Validate input
    var input phoneInput
    var brand models.PhoneBrand
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    if err := db.Where("id = ?", input.PhoneBrandID).First(&brand).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "PhoneBrandID not found!"})
        return
    }

    // Create Phone
    Phone := models.Phone{Name: input.Name, Year: input.Year, NetworkTechnology: input.NetworkTechnology, BodyDimensions: input.BodyDimensions, BodyWeight: input.BodyWeight, BodyBuild: input.BodyBuild, BodySIM: input.BodySIM, DisplayType: input.DisplayType, DisplaySize: input.DisplaySize, DisplayResolution: input.DisplayResolution, PlatformOS: input.PlatformOS, PlatformChipset: input.PlatformChipset,PlatformCPU: input.PlatformCPU,PlatformGPU: input.PlatformGPU,MemoryCard: input.MemoryCard,MemoryInternal: input.MemoryInternal,Camera: input.Camera,SoundLoudspeaker: input.SoundLoudspeaker,SoundJack: input.SoundJack,CommsWLAN: input.CommsWLAN,CommsBluetooth: input.CommsBluetooth,CommsGPS: input.CommsGPS,CommsUSB: input.CommsUSB, Battery: input.Battery, MiscColors: input.MiscColors, MiscPrice: input.MiscPrice, PhoneBrandID: input.PhoneBrandID}
    db.Create(&Phone)

    c.JSON(http.StatusOK, gin.H{"data": Phone})
}

// GetPhoneById godoc
// @Summary Dapatkan suatu model smartphone berdasarkan id.
// @Description Dapatkan 1 Phone berdasarkan id.
// @Tags Phone
// @Produce json
// @Param id path string true "Phone id"
// @Success 200 {object} models.Phone
// @Router /phones/{id} [get]
func GetPhoneById(c *gin.Context) { // Get model if exist
    var Phone models.Phone

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&Phone).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": Phone})
}

// UpdatePhone godoc
// @Summary Update data suatu model smartphone berdasarkan id.
// @Description Update Phone berdasarkan id.
// @Tags Phone
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Phone id"
// @Param Body body phoneInput true "body untuk update an Phone"
// @Success 200 {object} models.Phone
// @Router /phones/{id} [patch]
func UpdatePhone(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var Phone models.Phone
    var brand models.PhoneBrand
    if err := db.Where("id = ?", c.Param("id")).First(&Phone).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input phoneInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    if err := db.Where("id = ?", input.PhoneBrandID).First(&brand).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "PhoneBrandID not found!"})
        return
    }

    var updatedInput models.Phone
    updatedInput.Name = input.Name
    updatedInput.Year = input.Year
    updatedInput.NetworkTechnology = input.NetworkTechnology
    updatedInput.BodyDimensions = input.BodyDimensions
    updatedInput.BodyWeight = input.BodyWeight
    updatedInput.BodyBuild = input.BodyBuild
    updatedInput.BodySIM = input.BodySIM
    updatedInput.DisplayType = input.DisplayType
    updatedInput.DisplaySize= input.DisplaySize
    updatedInput.DisplayResolution = input.DisplayResolution
    updatedInput.PlatformOS = input.PlatformOS
    updatedInput.PlatformChipset = input.PlatformChipset
    updatedInput.PlatformCPU = input.PlatformCPU
    updatedInput.PlatformGPU = input.PlatformGPU
    updatedInput.MemoryCard = input.MemoryCard
    updatedInput.MemoryInternal = input.MemoryInternal
    updatedInput.Camera = input.Camera
    updatedInput.SoundLoudspeaker = input.SoundLoudspeaker
    updatedInput.SoundJack = input.SoundJack
    updatedInput.CommsWLAN = input.CommsWLAN
    updatedInput.CommsBluetooth = input.CommsBluetooth
    updatedInput.CommsGPS = input.CommsGPS
    updatedInput.CommsUSB = input.CommsUSB
    updatedInput.Battery = input.Battery
    updatedInput.MiscColors = input.MiscColors
    updatedInput.MiscPrice = input.MiscPrice
    updatedInput.PhoneBrandID = input.PhoneBrandID
    updatedInput.UpdatedAt = time.Now()

    db.Model(&Phone).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": Phone})
}

// GetRatingByRatingBrandId godoc
// @Summary Dapatkan seluruh rating yang telah diberikan terhadap smartphone dengan id tertentu.
// @Description Dapatkan seluruh rating yang telah diberikan terhadap smartphone dengan id tertentu.
// @Tags Phone
// @Produce json
// @Param id path string true "Phone id"
// @Success 200 {object} []models.Rating
// @Router /phones/{id}/ratings [get]
func GetRatingsByPhoneId(c *gin.Context) { // Get model if exist
    var Ratings []models.Rating

    db := c.MustGet("db").(*gorm.DB)

    if err := db.Where("phone_id = ?", c.Param("id")).Find(&Ratings).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": Ratings})
}

// GetOpinionByOpinionBrandId godoc
// @Summary Dapatkan seluruh opinion yang telah diberikan terhadap smartphone dengan id tertentu.
// @Description Dapatkan seluruh opinion yang telah diberikan terhadap smartphone dengan id tertentu.
// @Tags Phone
// @Produce json
// @Param id path string true "Phone id"
// @Success 200 {object} []models.Opinion
// @Router /phones/{id}/opinions [get]
func GetOpinionsByPhoneId(c *gin.Context) { // Get model if exist
    var Opinions []models.Opinion

    db := c.MustGet("db").(*gorm.DB)

    if err := db.Where("phone_id = ?", c.Param("id")).Find(&Opinions).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": Opinions})
}

// GetPriceByPriceBrandId godoc
// @Summary Get Dapatkan seluruh harga yang telah terdata terhadap smartphone dengan id tertentu.
// @Description Dapatkan seluruh harga yang telah terdata terhadap smartphone dengan id tertentu.
// @Tags Phone
// @Produce json
// @Param id path string true "Phone id"
// @Success 200 {object} []models.PhonePrice
// @Router /phones/{id}/prices [get]
func GetPricesByPhoneId(c *gin.Context) { // Get model if exist
    var Prices []models.PhonePrice

    db := c.MustGet("db").(*gorm.DB)

    if err := db.Where("phone_id = ?", c.Param("id")).Find(&Prices).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": Prices})
}

// GetImageByImageBrandId godoc
// @SummaryDapatkan seluruh gambar yang telah terdata terhadap smartphone dengan id tertentu.
// @Description Dapatkan seluruh gambar yang telah terdata terhadap smartphone dengan id tertentu.
// @Tags Phone
// @Produce json
// @Param id path string true "Phone id"
// @Success 200 {object} []models.PhoneImage
// @Router /phones/{id}/images [get]
func GetImagesByPhoneId(c *gin.Context) { // Get model if exist
    var Images []models.PhoneImage

    db := c.MustGet("db").(*gorm.DB)

    if err := db.Where("phone_id = ?", c.Param("id")).Find(&Images).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": Images})
}

// GetReviewByReviewBrandId godoc
// @Summary Dapatkan seluruh review yang telah diberikan terhadap smartphone dengan id tertentu.
// @Description Dapatkan seluruh review yang telah diberikan terhadap smartphone dengan id tertentu.
// @Tags Phone
// @Produce json
// @Param id path string true "Phone id"
// @Success 200 {object} []models.EditorReview
// @Router /phones/{id}/reviews [get]
func GetReviewsByPhoneId(c *gin.Context) { // Get model if exist
    var Reviews []models.EditorReview

    db := c.MustGet("db").(*gorm.DB)

    if err := db.Where("phone_id = ?", c.Param("id")).Find(&Reviews).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": Reviews})
}

// DeletePhone godoc
// @Summary Hapus sebuah model smartphone berdasarkan id.
// @Description Hapus satu Phone berdasarkan id.
// @Tags Phone
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "Phone id"
// @Success 200 {object} map[string]boolean
// @Router /phone/{id} [delete]
func DeletePhone(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var Phone models.Phone
    if err := db.Where("id = ?", c.Param("id")).First(&Phone).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&Phone)

    c.JSON(http.StatusOK, gin.H{"data": true})
}