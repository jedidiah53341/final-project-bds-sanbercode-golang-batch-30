package controllers

import (
    "fp_go_sanbercode30/models"
    "fmt"
    "time"
    "net/http"
    "github.com/gin-gonic/gin"
    "golang.org/x/crypto/bcrypt"
    "gorm.io/gorm"
)

type LoginInput struct {
    Username string `json:"username" binding:"required"`
    UserPassword string `json:"user_password" binding:"required"`
}

type RegisterInput struct {
    Username string `json:"username" binding:"required"`
    UserPassword string `json:"user_password" binding:"required"`
    UserEmail    string `json:"user_email" binding:"required"`
    Name        string    `json:"name"`
    UserRole        string    `json:"user_role"`       
}
type ChangePasswordInput struct {
    UserPassword string `json:"user_password" binding:"required"`
    NewUserPassword    string `json:"new_user_password" binding:"required"`    
}

// LoginUser godoc
// @Summary Login sebagai user.
// @Description Masukkan username dan password anda dan dapatkan token JWT yang diperlukan untuk setiap operasi POST, PATCH, dan DELETE.
// @Tags Auth
// @Param Body body LoginInput true "body untuk login a user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /login [post]
func Login(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var input LoginInput

    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    u := models.User{}

    u.Username = input.Username
    u.UserPassword = input.UserPassword

    token, err := models.LoginCheck(u.Username, u.UserPassword, db)

    if err != nil {
        fmt.Println(err)
        c.JSON(http.StatusBadRequest, gin.H{"error": "username or password is incorrect."})
        return
    }

    user := map[string]string{
        "username": u.Username,
        "email":    u.UserEmail,
        "user-role":    u.UserRole,
    }

    c.JSON(http.StatusOK, gin.H{"message": "login success", "user": user, "token": token})

}

// ChangePasswordUser godoc
// @Summary Ganti password user.
// @Description Cukup masukkan password baru di new_user_password.
// @Tags Auth
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body ChangePasswordInput true "body untuk change password of a user"
// @Produce json
// @Param id path string true "User id"
// @Success 200 {object} map[string]interface{}
// @Router /changepassword/{id} [patch]
func ChangePassword(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var User models.User
    if err := db.Where("id = ?", c.Param("id")).First(&User).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "User not found!"})
        return
    }

    // Validate input
    var input ChangePasswordInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    hashedPassword, errPassword := bcrypt.GenerateFromPassword([]byte(input.NewUserPassword), bcrypt.DefaultCost)
    if errPassword != nil {
        
    }
    var updatedInput models.User
    updatedInput.UserPassword = string(hashedPassword)
    updatedInput.UpdatedAt = time.Now()
    db.Model(&User).Updates(updatedInput)
    c.JSON(http.StatusOK, gin.H{"data": User})
}

// Register godoc
// @Summary Daftar seorang user.
// @Description Pastikan anda ingat user_password dan username yang anda masukkan, sebab akan diperlukan nanti ketika login.
// @Tags Auth
// @Param Body body RegisterInput true "body untuk register a user"
// @Produce json
// @Success 200 {object} map[string]interface{}
// @Router /register [post]
func Register(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)
    var input RegisterInput

    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    u := models.User{}

    u.Username = input.Username
    u.UserEmail = input.UserEmail
    u.UserPassword = input.UserPassword
    u.Name = input.Name
    u.UserRole = input.UserRole

    _, err := u.SaveUser(db)

    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    user := map[string]string{
        "username": input.Username,
        "email":    input.UserEmail,
        "name": input.Name,
        "user-role": input.UserRole,
    }

    c.JSON(http.StatusOK, gin.H{"message": "registration success", "user": user})

}