package controllers

import (
    "net/http"
    "time"
    "fp_go_sanbercode30/models"
    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
)

type phonePriceInput struct {
        Price        float64    `json:"price`
        Currency        string    `json:"currency"`
        Source        string    `json:"source"`
        UserID      uint   `json:"userID"`
        PhoneID      uint   `json:"phoneID"`
}

// GetAllPhonePrices godoc
// @Summary Dapatkan semua data harga untuk setiap model smartphone.
// @Description Dapatkan semua data harga untuk setiap model smartphone.
// @Tags PhonePrice
// @Produce json
// @Success 200 {object} []models.PhonePrice
// @Router /phone-prices/ [get]
func GetAllPhonePrice(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var PhonePrices []models.PhonePrice
    db.Find(&PhonePrices)

    c.JSON(http.StatusOK, gin.H{"data": PhonePrices})
}

// CreatePhonePrice godoc
// @Summary Masukkan sebuah data harga smartphone.
// @Description Untuk memasukkan data harga, masukkan mata uang yang digunakan di currency, link sumber dimana anda melihat harga tersebut di source, dan nilai harganya di price.
// @Tags PhonePrice
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body phonePriceInput true "body untuk membuat 1 PhonePrice."
// @Produce json
// @Success 200 {object} models.PhonePrice
// @Router /phone-prices/ [post]
func CreatePhonePrice(c *gin.Context) {
    db := c.MustGet("db").(*gorm.DB)

    // Validate input
    var input phonePriceInput
    var user models.User
    var phone models.Phone
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    if err := db.Where("id = ?", input.UserID).First(&user).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "UserID not found!"})
        return
    }

    if err := db.Where("id = ?", input.PhoneID).First(&phone).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "PhoneID not found!"})
        return
    }

    // Create PhonePrice
    PhonePrice := models.PhonePrice{Price: input.Price, Currency: input.Currency, Source: input.Source, UserID: input.UserID, PhoneID: input.PhoneID } 
    db.Create(&PhonePrice)

    c.JSON(http.StatusOK, gin.H{"data": PhonePrice})
}

// GetPhonePriceById godoc
// @Summary Dapatkan sebuah data harga berdasarkan id harganya.
// @Description Dapatkan sebuah data harga berdasarkan id harganya.
// @Tags PhonePrice
// @Produce json
// @Param id path string true "PhonePrice id"
// @Success 200 {object} models.PhonePrice
// @Router /phone-prices/{id} [get]
func GetPhonePriceById(c *gin.Context) { // Get model if exist
    var PhonePrice models.PhonePrice

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&PhonePrice).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": PhonePrice})
}

// UpdatePhonePrice godoc
// @Summary Update sebuah data harga berdasarkan id harganya.
// @Description Update sebuah data harga berdasarkan id harganya.
// @Tags PhonePrice
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "PhonePrice id"
// @Param Body body phonePriceInput true "body untuk update an PhonePrice"
// @Success 200 {object} models.PhonePrice
// @Router /phone-prices/{id} [patch]
func UpdatePhonePrice(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var PhonePrice models.PhonePrice
    var user models.User
    var phone models.Phone
    if err := db.Where("id = ?", c.Param("id")).First(&PhonePrice).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input phonePriceInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    if err := db.Where("id = ?", input.UserID).First(&user).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "UserID not found!"})
        return
    }

    if err := db.Where("id = ?", input.PhoneID).First(&phone).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "PhoneID not found!"})
        return
    }


    var updatedInput models.PhonePrice
    updatedInput.Price = input.Price
    updatedInput.Currency = input.Currency
    updatedInput.Source = input.Source
    updatedInput.UserID = input.UserID
    updatedInput.PhoneID = input.PhoneID
    updatedInput.UpdatedAt = time.Now()

    db.Model(&PhonePrice).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": PhonePrice})
}

// DeletePhonePrice godoc
// @Summary Hapus sebuah data harga berdasarkan id harganya.
// @Description Hapus sebuah data harga berdasarkan id harganya.
// @Tags PhonePrice
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "PhonePrice id"
// @Success 200 {object} map[string]boolean
// @Router /phone-prices/{id} [delete]
func DeletePhonePrice(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var PhonePrice models.PhonePrice
    if err := db.Where("id = ?", c.Param("id")).First(&PhonePrice).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&PhonePrice)

    c.JSON(http.StatusOK, gin.H{"data": true})
}