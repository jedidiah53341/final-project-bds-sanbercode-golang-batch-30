package controllers

import (
    "net/http"
    "time"
    "fp_go_sanbercode30/models"
    "github.com/gin-gonic/gin"
    "gorm.io/gorm"
)

type phoneBrandInput struct {
    Name        string `json:"name"`
}

// GetAllPhoneBrand godoc
// @Summary Dapatkan seluruh merk smartphone.
// @Description Dapatkan seluruh merk smartphone.
// @Tags PhoneBrand
// @Produce json
// @Success 200 {object} []models.PhoneBrand
// @Router /phone-brands [get]
func GetAllBrand(c *gin.Context) {
    // get db from gin context
    db := c.MustGet("db").(*gorm.DB)
    var Brands []models.PhoneBrand
    db.Find(&Brands)

    c.JSON(http.StatusOK, gin.H{"data": Brands})
}

// CreatePhoneBrand godoc
// @Summary Masukkan sebuah merk smartphone.
// @Description Masukkan sebuah merk smartphone.
// @Tags PhoneBrand
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Param Body body phoneBrandInput true "body untuk membuat 1 PhoneBrand"
// @Produce json
// @Success 200 {object} models.PhoneBrand
// @Router /phone-brands [post]
func CreateBrand(c *gin.Context) {
    // Validate input
    var input phoneBrandInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    // Create Brand
    Brand := models.PhoneBrand{Name: input.Name}
    db := c.MustGet("db").(*gorm.DB)
    db.Create(&Brand)

    c.JSON(http.StatusOK, gin.H{"data": Brand})
}

// GetPhoneBrandById godoc
// @Summary Dapatkan data sebuah merk smartphone berdasarkan id-nya.
// @Description Dapatkan data sebuah merk smartphone berdasarkan id-nya.
// @Tags PhoneBrand
// @Produce json
// @Param id path string true "PhoneBrand id"
// @Success 200 {object} models.PhoneBrand
// @Router /phone-brands/{id} [get]
func GetBrandById(c *gin.Context) { // Get model if exist
    var Brand models.PhoneBrand

    db := c.MustGet("db").(*gorm.DB)
    if err := db.Where("id = ?", c.Param("id")).First(&Brand).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": Brand})
}

// GetPhoneByPhoneBrandId godoc
// @Summary Dapatkan model-model smartphone berdasarkan id merk-nya.
// @Description Dapatkan model-model smartphone berdasarkan id merk-nya.
// @Tags PhoneBrand
// @Produce json
// @Param id path string true "PhoneBrand id"
// @Success 200 {object} []models.Phone
// @Router /phone-brands/{id}/phones [get]
func GetPhonesByBrandId(c *gin.Context) { // Get model if exist
    var Phones []models.Phone

    db := c.MustGet("db").(*gorm.DB)

    if err := db.Where("phone_brand_id = ?", c.Param("id")).Find(&Phones).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    c.JSON(http.StatusOK, gin.H{"data": Phones})
}

// UpdatePhoneBrand godoc
// @Summary Update data sebuah merk smartphone berdasarkan id-nya.
// @Description Update data sebuah merk smartphone berdasarkan id-nya.
// @Tags PhoneBrand
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "PhoneBrand id"
// @Param Body body phoneBrandInput true "body untuk update age Brand category"
// @Success 200 {object} models.PhoneBrand
// @Router /phone-brands/{id} [patch]
func UpdateBrand(c *gin.Context) {

    db := c.MustGet("db").(*gorm.DB)
    // Get model if exist
    var Brand models.PhoneBrand
    if err := db.Where("id = ?", c.Param("id")).First(&Brand).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    // Validate input
    var input phoneBrandInput
    if err := c.ShouldBindJSON(&input); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    var updatedInput models.PhoneBrand
    updatedInput.Name = input.Name
    updatedInput.UpdatedAt = time.Now()

    db.Model(&Brand).Updates(updatedInput)

    c.JSON(http.StatusOK, gin.H{"data": Brand})
}

// DeletePhoneBrand godoc
// @Summary Hapus data sebuah merk smartphone berdasarkan id-nya.
// @Description Hapus data sebuah merk smartphone berdasarkan id-nya.
// @Tags PhoneBrand
// @Param Authorization header string true "Anda harus login terlebih dahulu untuk mengakses endpoint ini. Masukkan token dengan format berikut : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Param id path string true "PhoneBrand id"
// @Success 200 {object} map[string]boolean
// @Router /phone-brands/{id} [delete]
func DeleteBrand(c *gin.Context) {
    // Get model if exist
    db := c.MustGet("db").(*gorm.DB)
    var Brand models.PhoneBrand
    if err := db.Where("id = ?", c.Param("id")).First(&Brand).Error; err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
        return
    }

    db.Delete(&Brand)

    c.JSON(http.StatusOK, gin.H{"data": true})
}