# Final Project Golang Backend Development 30
 Tema project Laravel saya adalah **review mobile phones**.
API ini mensimulasikan interaksi dimana user dapat memasukkan data smartphone dan merk smartphone. Setelah kedua data tersebut tersedia, user lainnya dapat memberikan ulasan berupa rating, opini, ataupun review. Selain itu, user juga dapat menyumbangkan gambar serta data harga dari masing - masing model smartphone. 

Pastikan untuk register dan login terlebih dahulu untuk mendapatkan token JWT-nya. Setelah login, sebaiknya POST terlebih dahulu beberapa merk telepon (PhoneBrand), sebab anda akan diminta memasukkan id merk telepon ketika POST data smartphone. Setelah merk dan model smartphone terdata, maka rating, opini, review, gambar, dan data harga dapat ditambahkan ke model smartphone tersebut.

 1. Final project sudah terupload di Heroku. Berikut adalah link Heroku-nya : https://phonereview-sb30-golang.herokuapp.com/swagger/index.html
 2. Database yang digunakan adalah Heroku Postgre. File SQL database adalah **db_fp_sb30_go.sql**. Berikut adalah gambar skema database-nya : ![Entity Relationship Diagram](./skemadatabase.jpg)
 3. Sudah terdapat fungsi register, login, dan ganti password, serta JWT middleware untuk setiap operasi POST, PATCH, dan DELETE.
 4. Interaksi user yang tersedia adalah CRUD model smartphone, merk smartphone, rating smartphone, review smartphone, opinion tentang smartphone, gambar, dan harga smartphone.
 5. Sudah terdiri dari 7 tabel.
 6. Dokumentasi API menggunakan Swagger UI.


