/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.4.22-MariaDB : Database - fp_go_sanbercode30
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`fp_go_sanbercode30` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `fp_go_sanbercode30`;

/*Table structure for table `editor_reviews` */

DROP TABLE IF EXISTS `editor_reviews`;

CREATE TABLE `editor_reviews` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `content` longtext DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `phone_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_phones_editor_review` (`phone_id`),
  KEY `fk_editor_reviews_user` (`user_id`),
  CONSTRAINT `fk_editor_reviews_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_phones_editor_review` FOREIGN KEY (`phone_id`) REFERENCES `phones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `editor_reviews` */

/*Table structure for table `opinions` */

DROP TABLE IF EXISTS `opinions`;

CREATE TABLE `opinions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `content` longtext DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `phone_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_opinions_user` (`user_id`),
  KEY `fk_phones_opinions` (`phone_id`),
  CONSTRAINT `fk_opinions_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_phones_opinions` FOREIGN KEY (`phone_id`) REFERENCES `phones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `opinions` */

/*Table structure for table `phone_brands` */

DROP TABLE IF EXISTS `phone_brands`;

CREATE TABLE `phone_brands` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` longtext DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `phone_brands` */

insert  into `phone_brands`(`id`,`name`,`created_at`,`updated_at`) values 
(1,'Xiaomi','2021-12-25 10:35:16.154','2021-12-25 10:35:16.154');

/*Table structure for table `phone_images` */

DROP TABLE IF EXISTS `phone_images`;

CREATE TABLE `phone_images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `image_src` longtext DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `phone_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_phone_images_user` (`user_id`),
  KEY `fk_phones_phone_images` (`phone_id`),
  CONSTRAINT `fk_phone_images_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_phones_phone_images` FOREIGN KEY (`phone_id`) REFERENCES `phones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `phone_images` */

/*Table structure for table `phone_prices` */

DROP TABLE IF EXISTS `phone_prices`;

CREATE TABLE `phone_prices` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `currency` longtext DEFAULT NULL,
  `price` double DEFAULT NULL,
  `source` longtext DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `phone_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_phone_prices_user` (`user_id`),
  KEY `fk_phones_phone_prices` (`phone_id`),
  CONSTRAINT `fk_phone_prices_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_phones_phone_prices` FOREIGN KEY (`phone_id`) REFERENCES `phones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `phone_prices` */

/*Table structure for table `phones` */

DROP TABLE IF EXISTS `phones`;

CREATE TABLE `phones` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` longtext DEFAULT NULL,
  `year` bigint(20) DEFAULT NULL,
  `network_technology` longtext DEFAULT NULL,
  `body_dimensions` longtext DEFAULT NULL,
  `body_weight` longtext DEFAULT NULL,
  `body_build` longtext DEFAULT NULL,
  `body_sim` longtext DEFAULT NULL,
  `display_type` longtext DEFAULT NULL,
  `display_size` longtext DEFAULT NULL,
  `display_resolution` longtext DEFAULT NULL,
  `platform_os` longtext DEFAULT NULL,
  `platform_chipset` longtext DEFAULT NULL,
  `platform_cpu` longtext DEFAULT NULL,
  `platform_gpu` longtext DEFAULT NULL,
  `memory_card` longtext DEFAULT NULL,
  `memory_internal` longtext DEFAULT NULL,
  `camera` longtext DEFAULT NULL,
  `sound_loudspeaker` longtext DEFAULT NULL,
  `sound_jack` longtext DEFAULT NULL,
  `comms_wlan` longtext DEFAULT NULL,
  `comms_bluetooth` longtext DEFAULT NULL,
  `comms_gps` longtext DEFAULT NULL,
  `comms_usb` longtext DEFAULT NULL,
  `battery` longtext DEFAULT NULL,
  `misc_colors` longtext DEFAULT NULL,
  `misc_price` longtext DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `phone_brand_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_phone_brands_phones` (`phone_brand_id`),
  CONSTRAINT `fk_phone_brands_phones` FOREIGN KEY (`phone_brand_id`) REFERENCES `phone_brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `phones` */

insert  into `phones`(`id`,`name`,`year`,`network_technology`,`body_dimensions`,`body_weight`,`body_build`,`body_sim`,`display_type`,`display_size`,`display_resolution`,`platform_os`,`platform_chipset`,`platform_cpu`,`platform_gpu`,`memory_card`,`memory_internal`,`camera`,`sound_loudspeaker`,`sound_jack`,`comms_wlan`,`comms_bluetooth`,`comms_gps`,`comms_usb`,`battery`,`misc_colors`,`misc_price`,`created_at`,`updated_at`,`phone_brand_id`) values 
(1,'Xiaomi Redmi 5A',2017,'GSM / CDMA / HSPA / LTE','140.4 x 70.1 x 8.4 mm (5.53 x 2.76 x 0.33 in)','137 g (4.83 oz)','Glass front, aluminum back, aluminum frame','Dual SIM (Nano-SIM, dual stand-by)','IPS LCD','5.0 inches, 68.0 cm2 (~69.1% screen-to-body ratio)','720 x 1280 pixels, 16:9 ratio (~296 ppi density)','Android 7.1.2 (Nougat), MIUI 11','Qualcomm MSM8917 Snapdragon 425 (28 nm)','Quad-core 1.4 GHz Cortex-A53','Adreno 308','microSDXC (dedicated slot)','16GB 2GB RAM, 32GB 3GB RAM','13 MP, f/2.2, PDAF','Yes','Yes','Wi-Fi 802.11 b/g/n, Wi-Fi Direct, hotspot','4.1, A2DP, LE','Yes, with A-GPS, GLONASS, BDS','microUSB 2.0','Li-Ion 3000 mAh, non-removable','Gold, Dark Gray, Rose Gold, Blue','About 100 EUR','2021-12-25 10:35:51.705','2021-12-25 10:35:51.705',1),
(2,'Xiaomi Redmi 6A',2018,'GSM / CDMA / HSPA / LTE','140.4 x 70.1 x 8.4 mm (5.53 x 2.76 x 0.33 in)','137 g (4.83 oz)','Glass front, aluminum back, aluminum frame','Dual SIM (Nano-SIM, dual stand-by)','IPS LCD','5.0 inches, 68.0 cm2 (~69.1% screen-to-body ratio)','720 x 1280 pixels, 16:9 ratio (~296 ppi density)','Android 7.1.2 (Nougat), MIUI 11','Qualcomm MSM8917 Snapdragon 425 (28 nm)','Quad-core 1.4 GHz Cortex-A53','Adreno 308','microSDXC (dedicated slot)','16GB 2GB RAM, 32GB 3GB RAM','13 MP, f/2.2, PDAF','Yes','Yes','Wi-Fi 802.11 b/g/n, Wi-Fi Direct, hotspot','4.1, A2DP, LE','Yes, with A-GPS, GLONASS, BDS','microUSB 2.0','Li-Ion 3000 mAh, non-removable','Gold, Dark Gray, Rose Gold, Blue','About 100 EUR','2021-12-25 10:36:25.068','2021-12-25 10:36:25.068',1);

/*Table structure for table `ratings` */

DROP TABLE IF EXISTS `ratings`;

CREATE TABLE `ratings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ux_score` bigint(20) DEFAULT NULL,
  `speed_score` bigint(20) DEFAULT NULL,
  `gaming_score` bigint(20) DEFAULT NULL,
  `standby_score` bigint(20) DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `phone_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users_ratings` (`user_id`),
  KEY `fk_phones_ratings` (`phone_id`),
  CONSTRAINT `fk_phones_ratings` FOREIGN KEY (`phone_id`) REFERENCES `phones` (`id`),
  CONSTRAINT `fk_users_ratings` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `ratings` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` longtext DEFAULT NULL,
  `username` longtext DEFAULT NULL,
  `user_email` longtext DEFAULT NULL,
  `user_password` longtext DEFAULT NULL,
  `user_role` longtext DEFAULT NULL,
  `created_at` datetime(3) DEFAULT NULL,
  `updated_at` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`username`,`user_email`,`user_password`,`user_role`,`created_at`,`updated_at`) values 
(1,'Jedidiah','jedidiah53341','jedidiah_ipb@apps.ipb.ac.id','$2a$10$TanL0yn0ODZUIxTTnYi2Vu4riz7PCWw/wE4GKY9GASx7UQJPAI1RO','Member','2021-12-25 10:34:01.166','2021-12-25 10:34:01.166');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
