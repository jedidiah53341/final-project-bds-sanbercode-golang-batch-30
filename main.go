package main

import (
    "fp_go_sanbercode30/config"
    "fp_go_sanbercode30/docs"
    "fp_go_sanbercode30/routes"
    "github.com/joho/godotenv"
    "log"
)

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @termsOfService http://swagger.io/terms/

func main() {
      // for load godotenv
    err := godotenv.Load()
    if err != nil {
        log.Fatal("Error loading .env file")
    }

  //programmatically set swagger info
  docs.SwaggerInfo.Title = "Golang REST API menggunakan Swagger - Sanbercode Batch 30"
  docs.SwaggerInfo.Description = "REST API ini bertemakan review smartphone. Kita mampu mendata spesifikasi smartphone mulai dari dimensinya, beratnya, hingga varian warnanya. Setiap model smartphone juga tergolong dalam sebuah merk/PhoneBrand. User dapat memberikan review berupa rating ( dengan format nilai 1 - 100 bilangan bulat ), opinion ( format string ), ataupun editorReview ( format string ). User juga dapat mengupload foto model smartphone tertentu. Terakhir, user mampu menambahkan harga smartphone, sambil mencantumkan link dimana harga tersebut tertera. Untuk setiap POST, PATCH, dan DELETE, user perlu login dan memberikan token terlebih dahulu."
  docs.SwaggerInfo.Version = "1.0"
  docs.SwaggerInfo.Host = "phonereview-sb30-golang.herokuapp.com"
  docs.SwaggerInfo.Schemes = []string{"http", "https"}

  db := config.ConnectDataBase()
  sqlDB, _ := db.DB()
  defer sqlDB.Close()

  r := routes.SetupRouter(db)
  r.Run()
}